#!/usr/bin/env python3

import argparse
import xml.etree.ElementTree as ET

def main():
    parser = argparse.ArgumentParser(description='Stitch together a single icon for pouet.net')
    parser.add_argument('template')
    parser.add_argument('icon')
    parser.add_argument('output')
    args = parser.parse_args()

    ET.register_namespace('', 'http://www.w3.org/2000/svg')

    # parse the inputs
    template = ET.parse(args.template)
    icon = ET.parse(args.icon)

    # strip svg tag from icon
    content = icon.findall("./*")

    # replace icon-placeholders
    for parent in template.findall(".//*[@id='icon-placeholder']/.."):
        plist = list(parent)
        for el in parent.findall("./*[@id='icon-placeholder']"):
            start_index = plist.index(el)
            for i, c in enumerate(content):
                parent.insert(start_index + i, c)
            parent.remove(el)

    with open(args.output, 'w') as f:
        template.write(f, encoding='unicode')


if __name__ == '__main__':
    main()
