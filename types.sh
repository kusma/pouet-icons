#!/bin/sh

mkdir -p output/types
for f in types/*.svg
do
    python ./icon.py templates/type.svg $f output/$f
done
