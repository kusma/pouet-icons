#!/bin/sh

mkdir -p output/platforms
for f in platforms/*.svg
do
    python ./icon.py templates/platform.svg $f output/$f
done
